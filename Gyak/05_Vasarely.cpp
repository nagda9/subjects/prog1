#include "graphics.hpp"
using namespace genv;

int main()
{
    gout.open(600,600);

    gout << color (0, 255, 0) << move (0, -255) << line (0, 510) << move (-255, -255) << line (510, 0) << move (-255, 0) << refresh;

    event ev;
    while (gin >> ev)
    {
        for (int i = 25; i > 0; i--)
        {
            int j = 25;
            int k = 255;
            gout << move_to (300, 300) << color (0, i*10, k-(i*10)) << move (0, -i*10) << line (-(j-(i-1))*10 , i*10) << move ((j-(i-1))*10, 0) << refresh;
            gout << move_to (300, 300) << color (0, i*10, k-(i*10)) << move (-i*10, 0) << line (i*10, (j-(i-1))*10) << move (0, -(j-(i-1))*10) << refresh;
            gout << move_to (300, 300) << color (0, i*10, k-(i*10)) << move (0, i*10) << line ((j-(i-1))*10 , -i*10) << move (-(j-(i-1))*10, 0) << refresh;
            gout << move_to (300, 300) << color (0, i*10, k-(i*10)) << move (i*10, 0) << line (-i*10, -(j-(i-1))*10) << move (0, (j-(i-1))*10) << refresh;

        }
    }
    return 0;
}
