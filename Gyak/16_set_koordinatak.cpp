#include <iostream>
#include <fstream>
#include <list>
#include <set>

using namespace std;

class Koord
{
private:
    float x, y;
    set<Koord> s;

public:
    void beolvas ()
    {
        ifstream bf ("koordinatak.txt");

        if (!bf.good())
        {
            cerr << "nem sikerult megnyitni a fajlt";
            exit(0);
        }

        Koord uj;

        while(bf.good())
        {
            bf >> uj.x >> uj.y;

            s.insert(uj);
        }

        for (set<Koord>::iterator sit = s.begin(); sit != s.end(); sit++)
        {
            cout << sit-> x << ' ' << sit -> y << endl;
        }
    }

    bool operator<(const Koord &a) const
    {
        return x < a.x || (x == a.x && y < a.y);
    }
};

int main()
{
    Koord koord;
    koord.beolvas();

    return 0;
}
