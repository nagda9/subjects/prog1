#include "graphics.hpp"
#include "iostream"
#include "vector"
#include "cmath"
#include "time.h"

using namespace genv;
using namespace std;

const int w = 60, h = 40;

struct Sejt
{
private:
    vector<vector<bool>> elet;

public:
    Sejt()
    {
        vector<vector<bool>> elet (w, vector<bool>(h, false));
        for (size_t i = 0; i < w; i++)
        {
            for (size_t j = 0; j < h; j++)
            {
                elet[i][j] = (rand() % 10) == 0; // �tlagosan minden 10. sejt lesz �lo
            }
        }
    }

    void rajzol(canvas& can)
    {
        can << color(255,255,255) << move_to(0,0) << box(w*10, h*10);

        for (size_t i = 0; i < w; i++)
        {
            for (size_t j = 0; j < h; j++)
            {
                can << move_to(i*10, j*10);

                if (elet[i][j] == true)
                {
                    gout << color(255,255,255) << box(10,10);
                }
                else
                {
                    gout << color(0,0,0) << box(10,10);
                }
            }
        }
    }

    int szomszedok(int i, int j)
    {
        int x = 0;
        for (int ver: {-1, 0, 1}) // a ciklusv�ltoz� �rt�kei -1 t�l 1-ig mennek
        {
            for (int hor: {-1, 0, 1})
            {
                if(!(ver == 0 && hor == 0)) // csak a szomsz�dait n�zz�k az adott sejtnek, �nmag�t nem
                {
                    if(elet[(i+ver+w) % w][(j+hor+h) % h] ==  1)
                    {
                        x += 1;
                    }
                }
            }
        }
        return x;
    }

    void frissit()
    {
        vector<vector<bool>> tmp (w, vector<bool>(h, false));

        for (size_t i=0; i < w; i++)
        {
            for (size_t j=0; j < w; j++)
            {
                int x = szomszedok(i, j);

                if (x < 2)
                {
                    tmp[i][j] = 0;
                }
                else if ((x == 2 || x ==3) && elet[i][j] == 1)
                {
                    tmp[i][j] = 1;
                }
                else if (x > 3)
                {
                    tmp[i][j] = 0;
                }
                else if (x == 3 && elet[i][j] == 0)
                {
                    tmp[i][j] = 1;
                }
                else
                {
                    tmp[i][j] = elet[i][j];
                }
            }
        }

        elet = tmp;

    }
};

int main()
{
    gout.open(w*10, h*10);
    srand(time(0));

    Sejt sejt;

    event ev;

    while(gin >> ev)
    {

        sejt.rajzol(gout);

        gout << refresh;
    }
    return 0;
}