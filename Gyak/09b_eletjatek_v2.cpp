#include "graphics.hpp"
#include <iostream>
#include <cmath>
#include <vector>
#include <fstream>

using namespace std;
using namespace genv;

class GameOfLife {
    int X, Y;
    int H, W;
    int r = 10;
    vector<vector<bool>> state_space;
    bool play = true;

    void init_state() { /// Random init.
        state_space.clear();
        for(int i = 0; i < X; i++) {
            vector<bool> tmp;
            for(int j = 0; j < Y; j++) {
                tmp.push_back(rand()%2);
            }
            state_space.push_back(tmp);
        }
    }

    void init_params(){
        W = X * r;
        H = Y * r;
        init_state();
    }

    void show_state() { /// adott �llapot megjelen�t�se
        gout << move_to(0,0) << color(0,0,0) << box(W,H) << color(255,255,255);
        for(int i = 0; i < X; i++) {
            for(int j = 0; j < Y; j++) {
                if(state_space[i][j]) {
                    gout << move_to(i*r, j*r) << box(r,r);
                }
            }
        }
    }

    int get_ind(int ind, int interval){ /// hat�r szab�ly - k�rbe �rjen a p�lya
        return (interval + (ind%interval)) % interval;
    }

    int get_neighbour_number(int x, int y){ /// Szomsz�dok sz�mol�sa
        int neighbour_num = 0;
        for(int i = -1; i < 2; i++){
            for(int j = -1 ; j < 2; j++){
                if (!(i == 0 && j == 0)) { // �nmag�t ne sz�molja
                    neighbour_num += state_space[get_ind(x+i,X)][get_ind(y+j,Y)];
                }
            }
        }
        return neighbour_num;
    }

    void calc_next_iteration(){
         vector<vector<bool>> next_state(X,(vector<bool>(Y,0)));
         for(int i = 0; i < X; i++) {
            for(int j = 0; j < Y; j++) {
                int neigh_num = get_neighbour_number(i, j);

                /// Game of Life szab�lyok alk.
                if(neigh_num == 2){
                    next_state[i][j] = state_space[i][j];
                } else if(neigh_num == 3){
                    next_state[i][j] = true;
                }
            }
        }
        state_space = next_state;
    }

    void change_state(int x, int y){
        state_space[x/r][y/r] = !state_space[x/r][y/r];
    }

public:
    GameOfLife() {
        X = 50;
        Y = 35;
        init_params();
    }

    GameOfLife(int x, int y) {
        X = x;
        Y = y;
        init_params();
    }

    void run() { // Csak az elegancia kedv��rt...
        gout.open(W,H);
        gin.timer(100);
        event ev;
        while(gin >> ev) {
            if(ev.button == btn_left){
                change_state(ev.pos_x, ev.pos_y);
            }
            if(ev.keycode == key_space){
                play = !play;
            }
            if(ev.keycode == key_escape){
                break;
            }
            if(ev.type == ev_timer){
                if(play)
                    calc_next_iteration();
                show_state();
            }
            gout << refresh;
        }
    }
};

int main() {
    GameOfLife game;
    game.run();
    return 0;
}
