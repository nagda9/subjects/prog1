#include "graphics.hpp"
#include "iostream"
#include "vector"
#include "cmath"

using namespace genv;
using namespace std;

const int w = 600, h = 600;


struct particle
{
private:
    int x, y, vx, vy;

//konstruktor
public:
    particle(int x0, int y0, int vx0, int vy0)  // : x = x0, y = y0, vx = vx0, vy = vy0 �gy is lehetne konstru�lni
    {
        x = x0;
        y = y0;
        vx = vx0;
        vy = vy0;
    }

    void rajzol(float t)
    {
        if (x + vx * t >= 0 && x + vx * t < w - 1 && y + vy * t + (t*t/2) >= 0 && y + vy * t + (t*t/2) < h - 1)
        {
            gout << color (255, 255, 255) << move_to(x + vx * t, y + vy * t + (t*t/2));
            gout << box(5,5);
        }
    }
};

int main()
{
    gout.open(w,h);

    particle p(0, 100, 10, -10);

    p.rajzol(0);
    gout << refresh;


    event ev;
    gin.timer(10);

    while(gin >> ev)
    {
        p.rajzol(ev.time / 100.0);
        gout << refresh;
    }
    return 0;
}