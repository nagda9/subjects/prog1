#include <iostream>
#include <vector>
#include <list>
#include <map>
#include <set>

using namespace std;

int main()
{
    /// vector
    /// elem el�r�s nagyon hat�kony
    vector<int> v;

    for (int i = 0; i < 10; i++)
    {
        v.push_back(i);
    }

    for (int i = 0; i < v.size(); i++)
    {
        cout << v[i] << " ";
    }
    cout << endl;

    for (int i: v)
    {
        cout << i << " ";
    }
    cout << endl;


    for (vector<int>::iterator vit = v.begin(); vit != v.end(); vit++)
    {
        cout << *vit << " ";
    }
    cout << endl;

    vector<int>::iterator vit;
    vit = v.begin();

    for (size_t i = 0; i < 3; i++)
    {
        vit++;
    }
    cout << *vit << endl;


    /// list: nem folytat�lagos mem�ria ter�let mint a vector, hanem az elemeket pointerek k�tik �ssze.
    /// elem besz�r�s nagyon hat�kony, ami vektorokra nem igaz, mivel a vektor �j elem besz�r�sa eset�n mindig lem�solja mag�t
    /// viszont az elemenk�nt el�r�s rosszabb, line�ris
    /// nem lehet indexelni

    list<int> l;

    for (int i = 0; i < 5; i++)
    {
        l.push_back(i);
    }

    /// �gy nem j�!
    /*
    for (int i = 0; i < l.size(); i++)
    {
        cout << l[i] << " ";
    }
    cout << endl;
    */

    /// �gy sem j�!
    /*
    for (int i: l)
    {
        cout << l << " ";
    }
    cout << endl;
    */

    /// iter�torral:
    for (list<int>::iterator lit = l.begin(); lit != l.end(); lit++)
    {
        cout << *lit << " ";
    }
    cout << endl;

    list<int>::iterator lit;
    lit = l.begin();
    for (size_t i = 0; i < 5; i++)
    {
        lit++;
    }
    cout << *lit << endl;

    /// set: szint�n nem tudjuk indexelni, de iter�torral v�gig tudunk rajta menni
    set<int> s;

    s.insert(3);
    s.insert(5);
    s.insert(7);

    for (set<int>::iterator sit = s.begin(); sit != s.end(); sit++)
    {
        cout << *sit << " ";
    }
    cout << endl;

    set<int>::iterator sit = s.find(5);
    //if (sit ==...


    /// map: kulcsok sorbarendez�se abc sorrendben
    /// valamilyen sz�veg �rt�khez sz�mot t�rolunk el

    map<string, int> m;
    m["januar"] = 31;
    m["februar"] = 29;
    m["marcius"] = 31;

    for (map<string, int>::iterator mit = m.begin(); mit != m.end(); mit++)
    {
        cout << mit -> first << " " << mit -> second << endl;
    }
    cout << endl;

    m["januar"] += 1;

    for (pair<string, int> i: m)
    {
        cout << i.first << " " << i.second << endl;
    }

    /// adott sz�veg, minden sz� elofordul�s�t szeretn�nk kigyujteni --> map

    return 0;
}
