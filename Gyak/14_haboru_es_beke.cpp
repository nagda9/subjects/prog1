#include <iostream>
#include <fstream>
#include <map>

using namespace std;

string kicsinyit (string str)
{
    string str_mod;
    for (int i=0; i <str.length();  i++)
    {
        if(str[i]!= '.' && str[i]!= '?' && str[i]!= '!' && str[i]!= ':'&& str[i]!= ',' && str[i]!= ';')
        {
            str_mod += tolower(str[i]);
        }
    }

    return str_mod;
}

map<string, int> beolvas (ifstream& bf)
{
    map<string, int> m;
    string str;

    while(bf.good())
    {
        bf >> str;
        str = kicsinyit(str);

        map<string, int>::iterator mit = m.find(str);

        if (mit == m.end())
        {
            m.insert({str, 1});
        }
        else
        {
            m[str] += 1;
        }
    }
    return m;
}

void kiir (map<string, int> m)
{
    for (pair<string, int> i: m)
    {
        cout << i.first << " " << i.second << endl;
    }
}

int main()
{
    ifstream bf ("haboruesbeke.txt");

    if (!bf.good())
    {
        cerr << "nem sikerult megnyitni a fajlt";
        exit(0);
    }

    map<string, int> m = beolvas(bf);

    kiir(m);

    return 0;
}
