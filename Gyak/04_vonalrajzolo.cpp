#include "graphics.hpp"
using namespace genv;


int main()
{
    gout.open(400,400);

    event ev;
    int i = 0;
    while(gin >> ev) {
        if(ev.button == btn_left && i == 0)
        {
            gout << move_to(ev.pos_x, ev.pos_y) << box(1, 1) << refresh;
            i++;
        }
        else if (ev.button == btn_left && i!= 0)
        {
            gout << line_to(ev.pos_x, ev.pos_y) << refresh;
        }
    }
    return 0;
}
