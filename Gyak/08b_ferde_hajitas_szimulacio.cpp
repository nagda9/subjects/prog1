#include "graphics.hpp"
#include "iostream"
#include "vector"
#include "cmath"

using namespace genv;
using namespace std;

const int w = 600, h = 600;

struct particle
{
private:
    float x, y, vx, vy;

public:
    particle(int x0, int y0, int vx0, int vy0)  // : x = x0, y = y0, vx = vx0, vy = vy0 �gy is lehetne konstru�lni
    {
        x = x0;
        y = y0;
        vx = vx0;
        vy = vy0;
    }

    void mozog(int ax, int ay, float dt)
    {
        vx += ax * dt;
        vy += ay * dt;

        x += vx * dt;
        y += vy * dt;
    }

    void rajzol(float t)
    {
        if (x >= 0 && x < w - 1 && y >= 0 && y < h - 1)
        {
            gout << color (255, 255, 255) << move_to(x, y);
            gout << box(5,5);
        }
    }
};

int main()
{
    gout.open(w,h);

    particle p(0, 100, 10, -10);

    event ev;
    gin.timer(10);

    while(gin >> ev)
    {
        if (ev.type == ev_timer)
        {
            p.mozog(0, 1, 0.1);
            p.rajzol(0);
        }
        gout << refresh;
    }
    return 0;
}