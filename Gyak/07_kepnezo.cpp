#include "graphics.hpp"
#include "fstream"
#include "iostream"
#include "vector"

using namespace genv;
using namespace std;


void beolvas (ifstream& bf, vector <vector <vector <int> > >& v)
{
    for (size_t i = 0; i < v.size(); i++)
    {
        for (size_t j = 0; j < v[i].size(); j++)
        {
            bf >> v[i][j][0] >> v[i][j][1] >> v[i][j][2];
        }
    }
}

void rajzol(int w, int h, vector<vector<vector<int>>>& v, int x_pos, int y_pos)
{
    gout << color (0,0,0) << move_to(0, 0) << box (w, h);

    for (size_t i = 0; i < v.size(); i++)
    {
        for (size_t j = 0; j < v[i].size(); j++)
        {
            gout << color ( v[i][j][0] , v[i][j][1] , v[i][j][2]);
            gout << move_to(x_pos - w/2 + j, y_pos - h/2 + i) << dot;
        }
    }
    gout << refresh;
}

int main()
{
    int w, h;

    ifstream bf;
    bf.open("a.kep");

    bf >> w >> h;

    vector <vector <vector <int> > > v (h, vector <vector <int> > (w, vector<int> (3))); //konstruktor

    beolvas (bf, v);
    gout.open(w,h);


    event ev;
    while (gin >> ev)
    {
        if(ev.type == ev_mouse)
        {
            rajzol(w, h, v, ev.pos_x, ev.pos_y);
        }
    }

    return 0;
}





