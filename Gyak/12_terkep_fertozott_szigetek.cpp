#include "graphics.hpp"
#include <iostream>
#include <cmath>
#include <vector>
#include <fstream>

using namespace std;
using namespace genv;

const double PI = 3.141592653589793238463;

class terkep {
    vector<vector<int>> mymap;
    vector<vector<int>> zmap;

    int X, Y;
    bool zombie = false;

    void clear_screen() {
        gout << move_to(0,0) << color(0,0,0) << box(X,Y);
    }

    void show_map() {
        for(int j = 0; j < Y; j++) {
            for(int i = 0; i < X; i++) {
                int height = mymap[i][j];
                gout << move_to(i,j);
                if(zmap[i][j]>0) {
                    int c = min(125 + height, 255);
                    gout << color(c, 0, 0);
                } else if( height < 0) {
                    int c = max(255 + height, 0);
                    gout << color(0, 0, c);
                } else {
                    int c = min(125 + height, 255);
                    gout << color(0, c, 0);
                }
                gout << dot;
            }
        }
    }

    void reset_zombie(){
        zmap = mymap;
        zmap.assign(X, vector<int> (Y, 0));
    }

    void zombie_alg() {
        vector<vector<int>> next_map(zmap);
        bool znext = false;
        for(int i = 0; i < X; i++) {
            for(int j = 0; j < Y; j++) {
                if(zmap[i][j]>0) {
                    for(auto & k : { -1, 0, 1 }) {
                        for(auto & l : { -1, 0, 1}) {
                            int x = i + k;
                            int y = j + l;
                            if(x >= 0 && x < X && y >=0 && y < Y){
                                if(mymap[x][y] >= 0 && !zmap[x][y]){
                                    next_map[x][y] = 1;
                                    znext = true;
                                }
                            }
                        }
                    }
                }
            }
        }
        zmap = next_map;
        zombie = znext;
    }

public:
    void load_map(string filename) {
        ifstream fin(filename);
        if(!fin.good())
            cerr << filename << " nem el�rheto!";
        fin >> X >> ws >> Y >> ws;
        for(int i = 0; i < X; i++) {
            vector<int> lin;
            for(int j = 0; j < Y; j++) {
                int a;
                fin >> a >> ws;
                lin.push_back(a);
            }
            mymap.push_back(lin);
        }
        reset_zombie();
    }

    terkep(string filename) {
        load_map(filename);
        gout.open(X,Y);
        gin.timer(10);
    }

    void show(event ev) {
        if (ev.type==ev_mouse) {
            if(ev.button == btn_left) {
                zombie = true;
                zmap[ev.pos_x][ev.pos_y] = 1;
            }
        }
        if(ev.keycode == key_space)
            reset_zombie();
        if(ev.type == ev_timer) {
            show_map();
            if(zombie) {
                zombie_alg();
            }

        }
    }
};

int main() {
    terkep domb("terkep.txt");
    event ev;
    while(gin >> ev) {
        domb.show(ev);
        gout << refresh;
    }
    return 0;
}
