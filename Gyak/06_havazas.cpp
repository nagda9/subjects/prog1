#include "graphics.hpp"
#include "stdlib.h"
#include "ctime"
#include "vector"

using namespace genv;
using namespace std;

const int w = 400, h = 400; /// glob�lis v�ltoz�knak konstansokat szoktunk haszn�lni, mint pl canvas magass�g �s sz�less�g

class snow  /// tagf�ggv�nyekkel kezelt�k �s be�ll�tottuk a l�that�s�gokat, ugyhogy ez m�r class, nem struct.
{
private:
    int x, y;

public:

    ///Konstruktort csin�lunk. Alapbol minden f�ggv�nynek van param�ter n�lk�li konstruktora, a v�ltoz�k kezd��rt�ket kapnak.
    snow()
    {
        x = rand() % w; //mivel w �s h glob�lisak, ez�rt tudjuk haszn�lni �ket, egy�bk�nt �t k�ne adni az �rt�keket
        y = rand() % h;
    }

    void mozog (int dx, int dy, int wind_)
    {
        x = (x + dx + w + wind_) % w; /// periodikus hat�rfelt�tel modulo haszn�lat�val: ha kil�g baloldalt, bej�n jobboldalt
        y = (y + dy + h) % h;
    }

    void rajzol (canvas& can)
    {
        can << color (255, 255, 255) << move_to (x, y);
        can << line(0, 5) << move_to (x,y);
        can << line(0, -5) << move_to (x,y);
        can << line(5, 0) << move_to (x,y);
        can << line(-5, 0) << move_to (x,y);
        can << line(-2, 3) << move_to (x,y);
        can << line(2, 3) << move_to (x,y);
        can << line(-2, -3) << move_to (x,y);
        can << line(2, -3) << move_to (x,y);
    }
};

int main()
{
    int n = 200;

    vector<snow> koord (n);
    // vector< vector<int> > koord(n, vector<int>(2)); ///megh�vjuk a vector konstruktor f�ggv�ny�t 2 param�terrel: m�ret (n), kezdo�rt�k (ami egy vector 1 param�terrel: m�ret (2))

    gout.open(w, h);
    event ev;
    gout << color(255, 255, 255);

    // srand(time(0));

    // int x = rand() % w, y = rand() % h; // koord.push_back({x,y}); ezzel is fel lehetne t�lteni a vectort

    for (size_t i=0; i<n; i++) // size_t elojel n�lk�li t�pus, nagy vektorokat lehet vele c�mezni
    {
        snow h;
        koord[i] = h;
        koord[i].rajzol(gout);
    }

    gout << refresh;
    gin.timer(50);
    int wind = 0;

    while(gin >> ev)
    {
        if (ev.type == ev_timer)
        {
            gout << color (0,0,0) << move_to(0, 0) << box (w, h);
            gout << color (255, 255, 255);

            for (size_t i = 0; i < koord.size(); i++)
            {
                koord[i].mozog (rand() % 7 - 3 , rand() % 10, wind); //a rand() mindig annyi f�le sz�mot gener�l, amennyi az els� sz�m, �s a m�sodik sz�m az intervallum eltol�s
                koord[i].rajzol(gout);
            }
            gout << refresh;
        }

        if (ev.type == ev_mouse)
        {
            wind = (ev.pos_x-(w/2))*6 / (w/2);
        }

    }
    return 0;
}
