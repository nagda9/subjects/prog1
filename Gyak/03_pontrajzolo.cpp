#include "graphics.hpp"
using namespace genv;


int main()
{
    gout.open(400,400);

    event ev;
    while(gin >> ev) {
        if(ev.button == btn_left)
        {
            gout << move_to(ev.pos_x, ev.pos_y) << box(2, 2) << refresh;
        }
    }
    return 0;
}