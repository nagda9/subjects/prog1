#include <fstream>
#include <iostream>
#include <vector>
#include <cstdlib>
#include "graphics.hpp"

using namespace std;
using namespace genv;

struct Terkep
{
private:
    int X, Y;
    vector<vector<int>> t;

public:
    Terkep(string filename)
    {
        ifstream bf (filename);

        bf >> Y >> X;

        for (size_t i=0; i < Y; i++) // Y helyett lehet t.size()
        {
            vector<int> tmp_sor;
            for (size_t j=0; j < X; j++) // X helyett lehet t[i].size()
            {
                int tmp;
                bf >> tmp;
                tmp_sor.push_back(tmp);
            }
            t.push_back(tmp_sor);
        }
    }

    void rajzol()
    {
        int mini = legkisebb();
        float skala = 255 / (legnagyobb()-mini);


        for (size_t i=0; i < Y; i++)
        {
            for (size_t j=0; j < X; j++)
            {
                int arnyek_x = 5 * (t[i][(j+1) % X] - t[i][j]); // az 5 �ll�that� param�ter
                int arnyek_y = 5 * (t[(i+1) % Y][j] - t[i][j]); // itt is
                int c = max(float(0), min((t[i][j]-mini)*skala + arnyek_x + arnyek_y, float(255)));
                gout << color (c, c, c) << move_to(j, i) << dot;
            }
        }
    }

    int legnagyobb() //maxker t�tel
    {
        int maxi = t[0][0];
        for (size_t i=0; i < t.size(); i++)
        {
            for (size_t j=0; j < t[i].size(); j++)
            {
                if(t[i][j] > maxi)
                {
                    maxi = t[i][j];
                }
            }
        }
        return maxi;
    }

    int legkisebb() //minker t�tel
    {
        int mini = t[0][0];
        for (size_t i=0; i < t.size(); i++)
        {
            for (size_t j=0; j < t[i].size(); j++)
            {
                if(t[i][j] < mini)
                {
                    mini = t[i][j];
                }
            }
        }
        return mini;
    }

    void megmutat(int y_pos)
    {

        int mini = legkisebb();


        for (size_t i=0; i < getX(); i++)
        {

            int ertek = t[y_pos][i];
            int kimenoy;

            kimenoy=getY()/2+ertek;



        gout << color (255,255,255)  << line_to(i,kimenoy);}

    }

    int getX()
    {
        return X;
    }

    int getY()
    {
        return Y;
    }

};




int main()
{
    Terkep terkep("terkep.txt");
    gout.open(terkep.getX(), terkep.getY());

    int i =0;
    bool a =0;

    event ev;
    while (gin >> ev)
    {

        if(ev.type == ev_mouse && ev.button == btn_left)
        {
            i++;
            a= i % 2;
        }

        if (a == 0)
        {
            terkep.rajzol();
            gout << refresh;
        }


        if (ev.type == ev_mouse && ev.button == btn_left && a == 1)
        {
            gout << color(0,0,0) << move_to(0,0) << box(terkep.getX(), terkep.getY()) << move_to (0,terkep.getY()/2);
            gout << color(255, 255,255) << line_to (terkep.getX(),terkep.getY()/2) << move_to (0,terkep.getY()/2);
            terkep.megmutat(ev.pos_y);
            gout<< refresh;
        }
    }
    return 0;
}
