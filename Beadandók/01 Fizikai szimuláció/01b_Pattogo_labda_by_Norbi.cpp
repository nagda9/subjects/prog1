#include "graphics.hpp"
#include <iostream>

using namespace genv;
using namespace std;

const double G = 0.2;
const double a = 0.9;


struct rugo
{
private:
    double x,y,mx,my,osztopont,y_alap, my_alap;

public:
    rugo()
    {
        mx = 50;
        x = (600-mx)/2;
        y = 499;
        my = 100;
        osztopont = 5;
        y_alap = y;
        my_alap = my;
    }

void kirajzolas()
{
    gout << color(155,155,155);
    gout << move_to(x,y) << line(mx,0);
    gout << move_to(x,y+my) << line(mx,0);

    for(int i = 0; i < osztopont; i++)
    {
        if(i%2 == 0)
        {
            gout << move_to(x, y+(my/osztopont)*i)
            << line_to(x+mx, y+(my/osztopont)*(i+1));
        }
        else
        {
            gout << move_to(x+mx, y+(my/osztopont)*i)
            << line_to(x, y+(my/osztopont)*(i+1));
        }
    }
    gout << color(255,255,255);
}

void rugoallando_valtoztatasa(event& ev)
{
    if(ev.keycode == key_up && osztopont < 10)
    {
        osztopont++;
    }
    if(ev.keycode == key_down && osztopont > 3)
    {
        osztopont--;
    }
}

void osszenyomodas(double labda_alja)
{
    if(labda_alja >= y_alap)
    {
        my -= labda_alja-y;
        y = labda_alja;
    }
}

double get_osztopont()
{
    return osztopont;
}

double get_nyomashatar()
{
    return y_alap +(my_alap/osztopont)*2;
}

};

struct labda
{
private:
    double x,y,r,vy;
public:

labda()
{
    r = 20;
    x = 300;
    y = 50;
    vy = 5;
}

void kirajzolas()
{
    gout << color(255,255,255);
    for(int i = -r; i < r; i++)
    {
        for(int j = -r; j < r; j++)
        {
            if(r*r >= i*i + j*j)
            {
                gout << move_to(x+i,y+j) << dot;
            }
        }
    }
}

void mozgas(double osztopont, double nyomashatar)
    {
        if(y + r + vy >= nyomashatar)
        {
            vy *= -1;
            vy *= a / (osztopont/3);
        }
        y += vy;
        vy += G;
    }

int alja()
{
    return y+r;
}
};


void torles()
{
    gout << move_to(0,0) << color(0,0,0)
    << box_to(599,599) << color(255,255,255);
}


int main()
{
    rugo R;
    labda L;
    gout.open(600,600);
    event ev;
    gin.timer(40);
    while(gin >> ev && ev.keycode != key_escape)
    {
        if(ev.type != ev_mouse)
        {
            R.kirajzolas();
            L.kirajzolas();
            gout << refresh;
            R.rugoallando_valtoztatasa(ev);
            L.mozgas(R.get_osztopont(), R.get_nyomashatar());
            R.osszenyomodas(L.alja());
            torles();
        }
    }
    return 0;
}
