#include "graphics.hpp"
#include <iostream>
#include <vector>
#include <cmath>
#include <sstream>
#include <stdlib.h>
#include <ctime>

using namespace genv;
using namespace std;

const int w = 400, h = 600;
const float g = 9.81;       // neh�zs�gi gyorsul�s
const float c = 2;          /// �ll�that� param�ter --> csillap�t�si t�nyez�

struct Rugo
{
private:
    int l;
    float m, D, y, dl, v;

public:
    Rugo()
    {
        l = 5;              /// �ll�that� param�ter --> rug� hossza (l = 20px)
        m = 50;             /// �ll�that� param�ter --> test t�mege (m = 20*20*20)
        D = 200;            /// �ll�that� param�ter --> rug��lland�
        y = 0;              /// �ll�that� param�ter
        v = 0;              /// Nem �ll�that�!
        dl = (m*g)/D;
    }

    void rezeg(float dt)
    {
        float a;
        a = (dl-y)*D/m - g - (c*v)/m;
        v += a*dt;
        y += v*dt;
    }

    void mozgat(int pos_y)
    {
        int k = round(pow(m, 1/3.0))*20;
        int x = pos_y - k/2 - 110;
        y = l + dl - x/20;
    }

    void rajzol(canvas& can)
    {
        int s = round(((l + dl - y)/l)*20/4); // megny�l�s
        int k = round(pow(m, 1/3.0))*20;
        string amplitudo, tomeg, rugo_hossz, rugoallando, csillapitas;
        stringstream ss;
        ss << y;
        ss >> amplitudo;
        ss.clear();
        ss << m;
        ss >> tomeg;
        ss.clear();
        ss << l;
        ss >> rugo_hossz;
        ss.clear();
        ss << D;
        ss >> rugoallando;
        ss.clear();
        ss << c;
        ss >> csillapitas;
        ss.clear();

        can << color (255,255,255) << move_to(0,0) << box(w,h);
        can << color (100,100,0) << move_to(0,0) << box(400, 50);

        can << color (0,0,0) << move_to (5, h - 100) << text("Csillap�t�s: " + csillapitas);
        can << color (0,0,0) << move_to (5, h - 70) << text("T�meg: " + tomeg + " kg");
        can << color (0,0,0) << move_to (5, h - 50) << text("Rug� hossza: " + rugo_hossz);
        can << color (0,0,0) << move_to (5, h - 30) << text("Rug��lland�: " + rugoallando + " N/m");
        can << color (0,0,0) << move_to (5, h - 10) << text("Amplit�d�: " + amplitudo);
        can << color (0,0,0) << move_to (w-110, h - 50) << text("Nyomd meg a\n'P'-t, hogy\nsz�neteltesd!");

        can << color (255,0,0) << move_to (0,round((l+dl)*20 + 80 + k/2)) << line(w, 0);
        can << color (0,0,0) << move_to(w/2, 50) << line (0, 30);



        for (int i = 0; i < l; i++)
        {
            can << line (-10, s) << line (10, s) <<  line (10, s) << line (-10,s);
        }

        can << line (0, 30);

        for (int i = -k/2; i <= k/2; i++)
        {
            for (int j = 0; j <= k; j++)
            {
                if(can.y() + j < h && pow(i, 2) + pow(j-k/2, 2) <= pow(k/2, 2))
                {
                    can << genv::move(i, j) << color (0,0,0) << dot << genv::move(-i, -j);
                }
            }
        }
    }
};


void pause(event& ev)
{
    if(ev.keycode == 'p')
    {
        while(gin >> ev && ev.keycode != 'p' )
        {
            if(ev.keycode == key_escape)
            {
                exit(0);
            }
        }
    }
}

int main()
{
    gout.open(w, h);

    Rugo rugo;
    event ev;
    gin.timer(10);

    while(gin >> ev && ev.keycode != key_escape)
    {
        if (!(ev.type == ev_mouse && ev.button == btn_left))
        {
            pause(ev);
            if (gin >> ev && ev.type == ev_timer)
            {
                rugo.rezeg(0.1);
                rugo.rajzol(gout);
                gout << refresh;
            }
        }

        if (ev.type == ev_mouse && ev.button == btn_left)
        {
            while(gin >> ev && ev.button != -btn_left)
            {
                if (ev.pos_y != 0)
                {
                    rugo.mozgat(ev.pos_y);
                    rugo.rajzol(gout);
                    gout << refresh;
                }
            }
        }
    }
    return 0;
}

