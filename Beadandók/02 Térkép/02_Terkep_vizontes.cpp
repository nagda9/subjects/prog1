/// 2. feladat, v�z �nt�se a t�rk�pre.

#include <fstream>
#include <iostream>
#include <vector>
#include <cstdlib>
#include "graphics.hpp"

using namespace std;
using namespace genv;

class Terkep
{
private:
    int X, Y;
    vector<vector<int>> t;
    vector<vector<int>> vizmelyseg;

public:
    Terkep(string filename)
    {
        load_map(filename);
        gout.open(X,Y);

        for (size_t i=0; i < t.size(); i++)
        {
            vector<int> tmp;
            for (size_t j=0; j < t[i].size(); j++)
            {
                int a = 0;
                tmp.push_back(a);
            }
            vizmelyseg.push_back(tmp);
        }
    }

    void load_map(string filename)
    {
        ifstream bf (filename);

        if(!bf.good())
        {
            cerr << filename << " nem tal�lhato.";
        }

        bf >> X >> ws >> Y >> ws;
        for(int i = 0; i < X; i++)
        {
            vector<int> tmp;
            for(int j = 0; j < Y; j++)
            {
                int a;
                bf >> a >> ws;
                tmp.push_back(a);
            }
            t.push_back(tmp);
        }
    }

    void rajzol()
    {
        int mini = legkisebb();
        int maxi = legnagyobb();
        float skala = 255 / (maxi-mini);

        for (size_t i=0; i < t.size(); i++)
        {
            for (size_t j=0; j < t[i].size(); j++)
            {
                int tengerszint = t[i][j] - vizmelyseg[i][j];
                int arnyek_x = 2 * (t[(i+1) % X][j] - t[i][j]);
                int arnyek_y = 2 * (t[i][(j+1) % Y] - t[i][j]);

                if (tengerszint < 0)
                {
                    unsigned char c = max((tengerszint-mini)*skala + arnyek_x + arnyek_y, float(0));
                    gout << color(0, 0, 125 + c);
                }
                else
                {
                    unsigned char c = min((t[i][j]-mini)*skala + arnyek_x + arnyek_y, float(255));
                    gout << color(0, c, 0);
                }

                gout << move_to(i,j) << dot;
            }
        }
    }

    int legnagyobb()
    {
        int maxi = t[0][0];
        for (size_t i=0; i < t.size(); i++)
        {
            for (size_t j=0; j < t[i].size(); j++)
            {
                if(t[i][j] > maxi)
                {
                    maxi = t[i][j];
                }
            }
        }
        return maxi;
    }

    int legkisebb()
    {
        int mini = t[0][0];
        for (size_t i=0; i < t.size(); i++)
        {
            for (size_t j=0; j < t[i].size(); j++)
            {
                if(t[i][j] < mini)
                {
                    mini = t[i][j];
                }
            }
        }
        return mini;
    }

    void frissit (int pos_x, int pos_y)
    {
        vector<vector<bool>> zombi (X, vector<bool>(Y, false));

        if (t[pos_x][pos_y] - vizmelyseg[pos_x][pos_y] < 0)
        {
            zombi[pos_x][pos_y] = true;
            terjedes(zombi);
        }
    }

    void terjedes(vector<vector<bool>> zombi)
    {
        vector<vector<bool>> next (zombi);
        vector<vector<bool>> previous (X, vector<bool>(Y, false));
        bool terjed = true;

        while (terjed)
        {
            terjed = false;
            for (int i = 0; i < X; i++)
            {
                for (int j = 0; j < Y; j++)
                {
                    if (zombi[i][j] == true && previous[i][j] == false)
                    {
                        for (int hor: { -1, 0, 1})
                        {
                            for (int ver: { -1, 0, 1})
                            {
                                int x = i + hor;
                                int y = j + ver;
                                if (0 <= x && x < X && 0 <= y && y < Y)
                                {
                                    if (zombi[x][y] == false && next[x][y] == false)
                                    {
                                        if (vizmelyseg[i][j] > t[x][y])
                                        {
                                            next[x][y] = true;
                                            terjed = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            previous = zombi;
            zombi = next;
        }

        for (int i = 0; i < X; i++)
        {
            for (int j = 0; j < Y; j++)
            {
                if (zombi[i][j] == true)
                {
                    vizmelyseg[i][j] += 1;
                }
            }
        }

        rajzol();
        gout << refresh;
    }
};

int main()
{
    Terkep terkep("terkep.txt");

    terkep.rajzol();
    gout << refresh;

    event ev;
    while (gin >> ev)
    {
        if (ev.type == ev_mouse && ev.button == btn_left)
        {
            terkep.frissit(ev.pos_x, ev.pos_y);
        }

    }
    return 0;
}
