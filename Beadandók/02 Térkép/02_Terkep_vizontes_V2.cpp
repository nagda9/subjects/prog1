/// 2. feladat, v�z �nt�se a t�rk�pre.

#include <fstream>
#include <iostream>
#include <vector>
#include <cstdlib>
#include "graphics.hpp"

using namespace std;
using namespace genv;

const int add_water = 5;

struct Position{
    int x, y;
    int magassag;
    int novekedes = 0;
    bool fertozes = 0;

    operator == (const Position &p) const
    {
        return x == p.x && y == p.y;
    }
};

class Terkep
{
private:
    int X, Y;
    vector<vector<Position>> t;

public:
    Terkep(string filename)
    {
        load_map(filename);
        gout.open(X,Y);
    }

    void load_map(string filename)
    {
        ifstream bf (filename);

        if(!bf.good())
        {
            cerr << filename << " nem talalhato";
        }

        bf >> X >> ws >> Y >> ws;
        for(int i = 0; i < X; i++)
        {
            vector<Position> tmp;
            for(int j = 0; j < Y; j++)
            {
                Position p;
                p.x = i;
                p.y = j;
                bf >> p.magassag >> ws;
                tmp.push_back(p);
            }
            t.push_back(tmp);
        }
    }

    void rajzol()
    {
        int mini = legkisebb();
        int maxi = legnagyobb();

        float skala = 255 / (maxi-mini);

        for (size_t i=0; i < t.size(); i++)
        {
            for (size_t j=0; j < t[i].size(); j++)
            {
                int tengerszint = t[i][j].magassag - t[i][j].novekedes;
                int arnyek_x = 2 * (t[(i+1) % X][j].magassag - t[i][j].magassag);
                int arnyek_y = 2 * (t[i][(j+1) % Y].magassag - t[i][j].magassag);

                if (tengerszint < 0)
                {
                    unsigned char c = max((tengerszint-mini)*skala + arnyek_x + arnyek_y, float(0));
                    gout << color(0, 0, 125 + c);
                }
                else
                {
                    unsigned char c = min((t[i][j].magassag-mini)*skala + arnyek_x + arnyek_y, float(255));
                    gout << color(0, c, 0);
                }

                gout << move_to(i,j) << dot;
            }
        }
    }

    int legnagyobb()
    {
        int maxi = t[0][0].magassag;
        for (size_t i=0; i < t.size(); i++)
        {
            for (size_t j=0; j < t[i].size(); j++)
            {
                if(t[i][j].magassag > maxi)
                {
                    maxi = t[i][j].magassag;
                }
            }
        }
        return maxi;
    }

    int legkisebb()
    {
        int mini = t[0][0].magassag;
        for (size_t i=0; i < t.size(); i++)
        {
            for (size_t j=0; j < t[i].size(); j++)
            {
                if(t[i][j].magassag < mini)
                {
                    mini = t[i][j].magassag;
                }
            }
        }
        return mini;
    }

    void frissit (int pos_x, int pos_y)
    {
        if (t[pos_x][pos_y].magassag - t[pos_x][pos_y].novekedes < 0)
        {
            Position p;
            p.x = pos_x;
            p.y = pos_y;

            vector<Position> zombi;
            zombi.push_back(p);

            vector<vector<Position>> t_copy = t;
            t_copy[p.x][p.y].fertozes = 1;
            t_copy[p.x][p.y].novekedes += add_water;

            terjedes(zombi, t_copy);
            t = t_copy;

            for (size_t i = 0; i < t.size(); i++)
            {
                for (size_t j = 0; j < t[i].size(); j++)
                {
                    t[i][j].fertozes = 0;
                }
            }
            rajzol();
        }
        //cout << t[pos_x][pos_y].novekedes << endl;
    }

    void terjedes (vector<Position> &zombi, vector<vector<Position>> &terkep)
    {
        int k = 0;
        while (true)
        {
            vector<Position> new_zombi = {};
            new_zombi = zombicsinalo(zombi, terkep);

            if(new_zombi.empty())
            {
                break;
            }
            else {
                zombi = new_zombi;
            }
            k++;
        }
    }

    vector<Position> zombicsinalo(vector<Position> &zombi, vector<vector<Position>> &terkep)
    {
        vector<Position> new_zombi = {};

        for (int n = 0; n < zombi.size(); n++)
        {
            for (int i = -1; i <= 1; i++)
            {
                for (int j = -1; j <= 1; j++)
                {
                    Position p;
                    p.x = zombi[n].x + i;
                    p.y = zombi[n].y + j;

                    if (p.x >= 0 && p.x < X && p.y >= 0 && p.y < Y)
                    {
                        if (terkep[p.x][p.y].fertozes == 0)
                        {
                            if (terkep[zombi[n].x][zombi[n].y].novekedes > terkep[p.x][p.y].magassag)
                            {
                                terkep[p.x][p.y].novekedes += add_water;
                                terkep[p.x][p.y].fertozes = 1;
                                new_zombi.push_back(p);
                                //gout << color(255,0,0) << move_to(p.x, p.y) << dot;
                            }
                        }
                    }
                }
            }
        }
        gout << refresh;
        return new_zombi;
    }
};

int main()
{
    Terkep terkep("terkep.txt");

    terkep.rajzol();
    gout << refresh;

    event ev;
    while (gin >> ev)
    {
        if (ev.type == ev_mouse && ev.button == btn_left)
        {
            terkep.frissit(ev.pos_x, ev.pos_y);
        }
    }
    return 0;
}
