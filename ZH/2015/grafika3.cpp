#include <graphics.hpp>
#include<iostream>
#include<fstream>
#include<vector>
#include <time.h>
#include <cmath>

using namespace std;
using namespace genv;

const int w = 400;
const int h = 400;
const double pi = 3.14;

void reset()
{
	gout << move_to(0,0) << color(0,0,0) << box(w,h); 
		int r1 = w/3;
		int r2 = w/2;
		
	for(double fi = 0; fi < 2*pi; fi += pi/6)
	{
		gout << color(255,255,255);
		int x1 = r1*cos(fi);
		int y1 = r1*sin(fi);
		int x2 = r2*cos(fi);
		int y2 = r2*sin(fi);
		gout << move_to(w/2+x1, h/2+y1) << line_to(w/2+x2, h/2+y2);
	}
}

void draw_sec(int sec)
{
	int r = w/3;
	int x = r*cos(-pi/2 + sec*(2*pi/60));
	int y = r*sin(-pi/2 + sec*(2*pi/60));
	gout << move_to(w/2, h/2) << line_to(w/2+x, h/2+y);
}

void draw_min(int min)
{
	int r = 155;
	int x = r*cos(-pi/2 + min*(2*pi/60));
	int y = r*sin(-pi/2 + min*(2*pi/60));
	for (int i = -2; i <= 2; i++)
	{
		gout << move_to(w/2, h/2) << line_to(w/2+x, h/2+y) << line_to(w/2+i, h/2+i);
	}
}

void draw_hr(int hr)
{
	int r = 100;
	int x = r*cos(-pi/2 + hr*(2*pi/12));
	int y = r*sin(-pi/2 + hr*(2*pi/12));
	for (int i = -3; i <= 3; i++)
	{
		gout << move_to(w/2, h/2) << line_to(w/2+x, h/2+y) << line_to(w/2+i, h/2+i);
	}
}

int main()
{
	gout.open(w,h);
	
	reset();

	time_t raw_time;
	time(&raw_time);
	int sec = raw_time%60;
	int min = (raw_time/60)%60;
	int hr = (raw_time/3600)%12;
	
	draw_sec(sec);
	draw_min(min);
	draw_hr(hr);
	gout << refresh;
	
	event ev;
	gin.timer(1000);
	while(gin >> ev && ev.keycode != key_escape)
	{
		if(ev.type == ev_timer)
		{
			time_t raw_time;
			time(&raw_time);
			int sec = raw_time%60;
			int min = (raw_time/60)%60;
			int hr = (raw_time/3600)%12;
			
			reset();
			draw_sec(sec);
			draw_min(min);
			draw_hr(hr);
			
			gout << refresh;
		}
	}
	return 0;
}
