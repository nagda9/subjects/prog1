#include <iostream>
#include <vector>
#include <map>
using namespace std;


#define ellenoriz(felt, pont) if (felt) pont++; else cerr << "Nem teljesul a feltetel a " << __LINE__<< ". sorban." <<endl;

/* Szabalyok:
    - csak az a beugró érvényes, amely nem tartalmaz semmilyen változtatást a main()-ben,
        és már nincs benne kommentezve semmi
    - az "ellenoriz"-en kívül nem tartalmaz #define sorokat, és azt sem szabad megváltoztatni
*/

// Innentol
struct C
{
    int c;
    int b = 0;

    C () {
        c = 33;
    }

    C (int num)
    {
        c = num;
    }

    C (int num1, int num2)
    {
        c = num1;
        b = num2;
    }

    bool operator< (const C & other_C) const {
        if (c < other_C.c) {
            return true;
        } else
        {
            return b < other_C.b;
        }
    }
};

int ezmegmi(int num1, int num2)
{
    return 2;
}

int ezmegmi(int num1, int num2, int num3)
{
    return 3;
}

int ezmegmi(int num1, int num2, int num3, int num4)
{
    return 4;
}

struct A
{
    vector<int> v = {10};

    vector<int>::iterator eleje()
    {
        return v.begin();
    }

    vector<int>::iterator vege()
    {
        return v.end();
    }
};

struct D {
    int * p;

    D(int &pp) {
        p = &pp;
    }

    ~D() {
        (*p)++;
    }
};

//Idáig

int main()
{
    int pont = 0;

    C c;
    ellenoriz(c.c==33, pont)

    map<C, int> m;
    m[C(1)]=2;
    m[C(1)]=1;
    ellenoriz(m.size()==1,  pont )
    ellenoriz(m.at(C(1))==1,  pont )

    m[C(2,1)]=3;
    m[C(1,2)]=2;
    m[C(1)]=1;
    m[C(1,2)]=2;
    m.erase(C(2,1));
    ellenoriz(m.size()==2,  pont )
    ellenoriz(m.at(C(1,0))==1 && m.at(C(1,2))==2,  pont )

    int egyik = ezmegmi(1,2);
    int masik = ezmegmi(1,2,3);
    int harmadik = ezmegmi(1,2,3,4);
    ellenoriz(egyik==2 && masik==3 && harmadik==4,  pont )

    A a;
    int sum=0;
    for (vector<int>::iterator it = a.eleje(); it!=a.vege(); ++it) {
        sum+=*it;
    }
    ellenoriz(sum==10,  pont )
    ellenoriz(sum==10,  pont ) // ez ket pontot er

    int pp=3;
    int pk;
    {
        D d(pp);
        pk = pp;
    }
    ellenoriz(pp!=pk, pont)
    ellenoriz(pp!=pk, pont) // ez ket pontot er

    cout << pont << "/10 pont" << endl;
    return 0;
}

