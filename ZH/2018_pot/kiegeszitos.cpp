#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

#define ellenoriz(felt, pont) if (felt) pont++; else cerr << "Nem teljesul a feltetel a " << __LINE__<< ". sorban." <<endl;

/* Szab�lyok:
    - csak az a beugr� �rv�nyes, amely nem tartalmaz semmilyen v�ltoztat�st a main()-ben,
        �s m�r nincs benne kommentezve semmi az "//idaig" sor ut�n
    - az "ellenoriz"-en k�v�l nem tartalmaz #define sorokat, �s azt sem szabad megv�ltoztatni
    - �j #include megengedett a C/C++ standard k�nyvt�rbeli elemek k�z�l
*/

// Innentol

// Idaig

int main()
{
    int pont = 0;
    srand(time(0));
/*
    A a;
    ellenoriz(a.a=='a', pont);
    ellenoriz(a.aa.a=='a', pont);

    string s;
    s+='a'+rand()%26;
    s+='a'+rand()%26;
    Be be(s);
    ellenoriz (be.s == s, pont);
    ellenoriz (be.s == s, pont);

    map<A, Be> ce;
    ce[a] = be;
    ellenoriz (ce.size()==1, pont);
    ellenoriz (ce.size()==1, pont);

    string varazslat = csiriba();
    ellenoriz (varazslat == s, pont);
    ellenoriz (varazslat == s, pont);

    x = rand()%100;
    ellenoriz(y == x, pont);
    ellenoriz(y == x, pont);
*/
    cout << endl << pont << "/10 pont";

}
