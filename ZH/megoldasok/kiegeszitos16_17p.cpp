#include <iostream>
#include <sstream>
#include <vector>
#include <set>
#include <algorithm>

using namespace std;

#define ellenoriz(felt, pont) if (felt) pont++; else cerr << "Nem teljesul a feltetel a " << __LINE__<< ". sorban." <<endl;

/* Szabalyok:
    - csak az a beugr� �rv�nyes, amely nem tartalmaz semmilyen v�ltoztat�st a main()-ben,
        ami vagy nem tartalmaz komment blokkot, vagy a komment blokk v�ge ott marad ahol van �s csak az eleje megy lejjebb, �s csak ez az egyetlen, �sszef�ggo komment blokk van a main()-ben.
    - az "ellenoriz"-en k�v�l nem tartalmaz #define sorokat, �s azt sem szabad megv�ltoztatni
*/

// Innentol

int hatezmi;

struct A{
    int x;

    A(){
        x = 42;
        hatezmi = 4;
    }

    A(int X){
        x = X;
    }

    bool operator < (const A masik)const{
        return x < masik.x;
    }

    void setB(int X){
        x = X;
    }

    int& getB(){
        return x;
    }

    ~A(){
        hatezmi = 3;
    }
};


// Idaig

int main()
{
    int pont = 0;
    A a;
    ellenoriz(a.x==42 , pont)

    A b(1337);
    ellenoriz(b.x==1337 , pont)

    set<A> s;
    s.insert(a);
    s.insert(b);
    s.insert(a);
    ellenoriz(s.size()==2, pont)

    a.setB(2);
    b.setB(1);
    ellenoriz(a.getB()==2 && b.getB()==1, pont)

    b.getB()=3;
    ellenoriz(b.getB()==3, pont)
    ellenoriz(b.getB()==3, pont)

    {
        A c;
        hatezmi = 4;
    }
    ellenoriz(hatezmi == 3, pont)
    ellenoriz(hatezmi == 3, pont)


    A d;
    ellenoriz(hatezmi == 4, pont)
    ellenoriz(hatezmi == 4, pont)



    cout << pont << " / 10 pont" << endl;
    return 0;
}
