/**
*   B)
*
*   Ebben a feladatrészben a bábúkat setben tároltam, hogy lássuk, mennyivel egyszerűbb, ha kihasználjuk a set tulajdonságait
*   A bábúk színeit a koord tructban tároltam
*
*/


#include <iostream>
#include <set>
#include <list>
#include <cmath>

using namespace std;

struct koord{
    int x;
    int y;

    char szin;

    koord(int X, int Y, char c) {
        szin = c;
        x = X;
        y = Y;
    }

    bool operator < (const koord k)const{
        if(szin != k.szin)
            return szin < k.szin;
        if(x != k.x)
            return x < k.x;
        return y < k.y;
    }

};

struct Lepes
{
private:

public:
    list<koord> lepesek;

    Lepes(int x, int y, char c)
    {
        koord k(x,y,c);
        lepesek.push_back(k);
    }

    void lep(int x, int y)
    {
        char c = lepesek.begin()->szin;
        koord k(x,y,c);
        lepesek.push_back(k);
    }

    list<koord> getLepesek() {
        return lepesek;
    }
};

struct Dama
{
private:
    set<koord> babuk;

public:
    void uj_babu(int x, int y, char c) //w: white, b: black
    {
        koord k(x,y,c);
        babuk.insert(k);
    }

    bool szabalyos_e(Lepes lp)
    {
        //cout <<"lpsize: " << lp.lepesek.size() << endl;
        list<koord>::iterator elso = lp.lepesek.begin();
        list<koord>::iterator masodik = lp.lepesek.begin();
        masodik++;
        while(masodik != lp.lepesek.end()) {
            int x1,x2,y1,y2;
            x1 = (*elso).x;
            x2 = (*masodik).x;
            y1 = (*elso).y;
            y2 = (*masodik).y;
            if(abs(x2-x1) == (y2-y1) && 0 < (y2-y1) && (y2-y1) <=2) {
                if(y2-y1 == 2) { //Ha ugrik
                    if(!van_e_babu(x1+((x2-x1)/2), y2-1)) {
                        return false;
                    }
                }
                if(van_e_babu(x2,y2)) {
                    return false;
                }
            }
            else {return false;}
            elso++;
            masodik++;
        }
        return true;
    }

    bool van_e_babu(int x, int y){
        for(koord b : babuk) {
            if(b.x == x && b.y == y) {
                return true;
            }
        }
        return false;
    }

    void meglep(Lepes lp){

        //Mivel a setben lévő koordináták értékeit nem lehet megváltoztatni, eztéért töröljük a régi koordinátát és bele teszünk egy újat
        list<koord>::iterator elso = lp.lepesek.begin();
        list<koord>::iterator masodik = lp.lepesek.begin();
        masodik++;
        while(masodik != lp.lepesek.end()) {
            if(abs(elso->x - masodik->x) == 2){ // && abs(elso->y - masodik->y) == 2
                int xatugrott = (elso->x+masodik->x)/2;
                int yatugrott = (elso->y+masodik->y)/2;
                koord atugrott(xatugrott, yatugrott, elso->szin == 'b' ? 'w' : 'b'); //csinálunk egy olyan koordinátátjú bábút, amit átugrottunk, de ellenkező színnel, mint amilyennel lépünk
                if(babuk.find(atugrott) != babuk.end()){ //ha volt ilyen bábu, akkor kitöröljük
                    babuk.erase(babuk.find(atugrott));
                }
            }
            elso++;
            masodik++;
        }

        list<koord>::iterator elsoElem = lp.lepesek.begin();
        list<koord>::iterator utolsoElem = lp.lepesek.end();
        utolsoElem--;
        if(babuk.find((*elsoElem)) != babuk.end())
            babuk.erase(babuk.find(*elsoElem));
        babuk.insert(*utolsoElem);
    }

    void kiir() {
        cout << "\nBabuk:\n";
        for(koord k : babuk) {
            cout << "\t" << k.x << " " << k.y << ": " << k.szin <<"\n";
        }
    }

};


int main()
{
    Dama d;
    d.uj_babu(1,1,'b'); ///x,y koordináták, az y nő az ellenfél felé
    d.uj_babu(0,0,'w');
    d.uj_babu(2,0,'w');
    Lepes lp(1,1,'w'); /// az 1,1 koordinátán álló bábu akar majd lépni
    lp.lep(2,2);
    cout << d.szabalyos_e(lp); /// "1",  szabályos lépés, egy átlós előre lépés. A szabályosságvizsgálat NEM lépi meg a lépést
    Lepes lp2(0,0,'w');
    lp2.lep(2,2);
    cout << d.szabalyos_e(lp2); /// "1",  szabályos ugrás, egy saját bábut (1,1) átlós előre átugrás.
    d.uj_babu(1,3,'b');
    lp2.lep(0,4); /// lp2 ugrássorozat folytatódik
    cout << d.szabalyos_e(lp2); /// "1",  szabályos ugrássorozat, az (1,1)-et átugorva (2,2)-re, onnan (1,3)-at átugorva (0,4)-re érkezünk
    d.kiir();
    d.meglep(lp2); /// az lp lépés megtétele
    d.kiir();
    cout << d.szabalyos_e(lp2); /// "0",  szabálytalan ugrássorozat, az (1,1)-et átugorva juthatnánk (2,2)-re, de az (1,1)-en már nincs bábu
    Lepes lp3(2,2,'w');
    lp3.lep(3,3);
    cout << d.szabalyos_e(lp3); /// "1"
    return 0;
}

