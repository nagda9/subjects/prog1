/**
* 17-18 pótzh (Prog1)
*
*/

#include <iostream>
#include <map>
#include <vector>

using namespace std;

struct Festes{
private:
    ///Itt lehet elég lenne csak map< string, int >
    map<string, vector<int>> hozzavalok;    // Ennyi  és ilyen festék kell az adott festéshez

public:
    void kellBele(string tipus, int mennyi){
        hozzavalok[tipus].push_back(mennyi);
    }

    map<string, vector<int>> getHozzavalok(){
        return hozzavalok;
    }
};

struct Raktar{
private:
    map<string, vector<int>> festekek; //map< tipus, vector< hany %-ig van feltöltve > >
public:
    void ujFestek(string tipus){
        festekek[tipus].push_back(100);
    }

    void dump(){
        for(pair<string, vector<int>> p : festekek){
            cout << p.first << ": ";
            for(int a : p.second){
                cout << a << " ";
            }
        }
        ///iterátorosan:
//        for(map<string, vector<int>>::iterator it = festekek.begin(); it != festekek.end(); it++){
//            cout << it->first << ": ";
//            for(int i = 0; i < it->second.size(); i++){
//                cout << (it->second)[i] << " ";
//            }
//        }
        ///A két forciklust lehet keverni egymással:
//        for(map<string, vector<int>>::iterator it = festekek.begin(); it != festekek.end(); it++){
//            cout << it->first << ": ";
//            for(int a : it->second){
//                cout << a << " ";
//            }
//        }
        ///A két forciklust lehet keverni egymással:
//        for(pair<string, vector<int>> p : festekek){
//            cout << p.first << ": ";
//            for(int i = 0; i < p.second.size(); i++){
//                cout << (p.second)[i] << " ";
//            }
//        }

        cout << endl;
    }

    bool vanRaktaron(string tipus, int ennyiKell){
        for(int ennyiVan: festekek[tipus]){
            if(ennyiKell <= ennyiVan)
                return true;
        }
        return false;
    }

    bool elegendo(Festes F){
        for(pair<string, vector<int>> p : F.getHozzavalok()){ //Végigmegyünk, hogy megnézzük milyen festékek kellenek
            for(int ennyiKell : p.second){//Megnézzük mennyi kell belőle
                if(!vanRaktaron(p.first, ennyiKell))//Ha nincs annyi raktáron (ott a ! a fv előtt)
                    return false;
            }
        }
        return true;
    }

    void levonas(string tipus, int ennyiKell){
        int minimumTobblet = 101; //Indulunk a maximum lehetségestől eggyel nagyobb értékről, és folyamatosan csökkentjük ha találunk nála kisebbet
        int minimumIndex;
        for(int i = 0; i < festekek[tipus].size(); i++){
            int ennyiVan = festekek[tipus][i];
            if(ennyiVan >= ennyiKell && ennyiVan-ennyiKell < minimumTobblet){
                minimumTobblet = ennyiVan-ennyiKell;
                minimumIndex = i;
            }
        }

        //Megvan, hogy melyik festékből vonunk le és le is vonjuk
        festekek[tipus][minimumIndex] -= ennyiKell;

    }

    void fest(Festes F){
        for(pair<string, vector<int>> p : F.getHozzavalok()){ //Végigmegyünk, hogy megnézzük milyen festékek kellenek
            for(int ennyiKell : p.second){//Megnézzük mennyi kell belőle
                levonas(p.first, ennyiKell);//Levonjuk a festékek közül
            }
        }
    }

    int legkisebbFestesMennyisege(vector<Festes> festesek, string tipus){
        int minimum = 100;
        for(Festes F : festesek){
            vector<int> lehetosegek = F.getHozzavalok()[tipus];

            for(int ennyiKell : lehetosegek){
                if(ennyiKell < minimum)
                    minimum = ennyiKell;
            }
        }
        return minimum;
    }

    void osszeont(vector<Festes>& festesek){///Ez a rész kicsit zavaros, hogy hogy érti a feladat, lehet, hogy nem ezek a pontos feltételek
        for(pair<string, vector<int>> p : festekek){
            vector<int> masolataTipusrol = p.second;
            int minMennyiseg = legkisebbFestesMennyisege(festesek, p.first);
            int sum = 0;

            for(int maradek : p.second){
                if(maradek < minMennyiseg){
                    sum += maradek;
                    if(sum > 100)
                        break;
                }else
                    masolataTipusrol.push_back(maradek);
            }
            if(100-minMennyiseg > sum){
                break;
            }
            masolataTipusrol.push_back(sum);
            //Ha az osszeontottek nem érik el a 100-minMennyiseget, akkor nem öntjük össze
            //Egyébként összeöntjük:
            festekek[p.first] = masolataTipusrol;


        }


    }

    void elorelatas(vector<Festes> festesek, unsigned N){//C rész
        ///Nincs kész
        //Itt egy szimulációt kéne írni, ami megnézi az összes kombinációt n festésre és mindegyik tipusból a legtöbbet fogyasztóval számol
    }

};





int main()
{
    Raktar r;
    r.ujFestek("A");
    r.ujFestek("A");
    r.ujFestek("B");
    r.dump(); //A: 100 100   B:100 -----------------------------Ez a sor el volt írva!!-----------------------------
    Festes f1;
    f1.kellBele("A", 30);
    f1.kellBele("B", 40);
    Festes f2;
    f2.kellBele("A",60);
    f2.kellBele("B",10);

    vector<Festes> festesek;
    festesek.push_back(f1);
    festesek.push_back(f2);

    cout << r.elegendo(f1) << endl; //1
    r.fest(f1);
    r.dump();
    r.osszeont(festesek);
    r.dump(); //A: 70 100  B:60
    cout << r.elegendo(f2) << endl; //1
    r.fest(f2);
    r.dump();
    r.osszeont(festesek);
    r.dump(); //A: 10 100  B:50
    cout << r.elegendo(f2) << endl; //1
    r.fest(f2);
    r.dump();
    r.osszeont(festesek);
    r.dump(); //A: 10 40  B:40
    cout << r.elegendo(f2) << endl; //0
    cout << r.elegendo(f1) << endl; //1
    r.fest(f1);
    r.dump();
    r.osszeont(festesek);
    r.dump(); //A: 10 10  B:0

    r.elorelatas(festesek,2);///Ez a C rész
    r.dump();

    return 0;
}
