#include <iostream>
#include <vector>
#include <map>
using namespace std;


#define ellenoriz(felt, pont) if (felt) pont++; else cerr << "Nem teljesul a feltetel a " << __LINE__<< ". sorban." <<endl;

/* Szabalyok:
    - csak az a beugr� �rv�nyes, amely nem tartalmaz semmilyen v�ltoztat�st a main()-ben,
        �s m�r nincs benne kommentezve semmi
    - az "ellenoriz"-en k�v�l nem tartalmaz #define sorokat, �s azt sem szabad megv�ltoztatni
*/

// Innentol

struct C{
    int c,d;

    C(){
        c = 33;
        d = 0;
    }

    C(int ertek){
        c = ertek;
        d = 0;
    }

    C(int ertek, int D){
        c = ertek;
        d = D;
    }

    bool operator < (const C masik)const{
        if(c != masik.c)
            return c < masik.c;
        return d < masik.d;
    }
};

int ezmegmi(int a, int b){
    return 2;
}

int ezmegmi(int a, int b, int c){
    return 3;
}

int ezmegmi(int a, int b, int c, int d){
    return 4;
}


struct A{
    vector<int> v;

    A(){
        v.push_back(10);
    }

    vector<int>::iterator eleje(){
        return v.begin();
    }

    vector<int>::iterator vege(){
        return v.end();
    }
};

struct D{
    int* pp;

    D(int &valt){
        pp = &valt;
    }

    ~D(){
        (*pp)--;
    }
};

// Idaig




int main()
{
    int pont = 0;
    C c;
    ellenoriz(c.c==33, pont)

    map<C, int> m;
    m[C(1)]=2;
    m[C(1)]=1;
    ellenoriz(m.size()==1,  pont )
    ellenoriz(m.at(C(1))==1,  pont )

    m[C(2,1)]=3;
    m[C(1,2)]=2;
    m[C(1)]=1;
    m[C(1,2)]=2;
    m.erase(C(2,1));
    ellenoriz(m.size()==2,  pont )
    ellenoriz(m.at(C(1,0))==1 && m.at(C(1,2))==2,  pont )

    int egyik = ezmegmi(1,2);
    int masik = ezmegmi(1,2,3);
    int harmadik = ezmegmi(1,2,3,4);
    ellenoriz(egyik==2 && masik==3 && harmadik==4,  pont )

    A a;
    int sum=0;
    for (vector<int>::iterator it = a.eleje(); it!=a.vege(); ++it) {
        sum+=*it;
    }
    ellenoriz(sum==10,  pont )
    ellenoriz(sum==10,  pont ) // ez ket pontot er

    int pp=3;
    int pk;
    {
        D d(pp);
        pk = pp;
    }
    ellenoriz(pp!=pk, pont)
    ellenoriz(pp!=pk, pont) // ez ket pontot er

    cout << pont << "/10 pont" << endl;
    return 0;
}
