#include <iostream>
#include <cstdlib>
#include <ctime>
#include <cmath>
#include <vector>
#include <fstream>
#include <set>
#include <map>
#include <algorithm>
using namespace std;

#define ellenoriz(felt, pont) if (felt) pont++; else cerr << "Nem teljesul a feltetel a " << __LINE__<< ". sorban." <<endl;

/* Szabályok:
    - csak az a beugró érvényes, amely nem tartalmaz semmilyen változtatást a main()-ben,
        és már nincs benne kommentezve semmi az "//idaig" sor után
    - az "ellenoriz"-en kívül nem tartalmaz #define sorokat, és azt sem szabad megváltoztatni
    - új #include megengedett a C/C++ standard könyvtárbeli elemek közül
*/

// Innentol
struct Ember{
    string nev;
    bool klon; //Azért, hogy két külön egyforma nevű embert tárolhassunk kell egy megkülönböztető mezőt rakni (egy int-et, vagy jelen esetben egy bool-t tettem) viszont ezt az operátorban is meg kell jeleníteni

    Ember(){
        klon = false;
        nev = "Lajos";
    }

    Ember(string Nev){
        klon = false;
        nev = Nev;
    }

    bool operator < (const Ember E)const{
        if(nev != E.nev)
            return nev < E.nev; // vagy: return this->nev < E.nev
        return klon < E.klon; //Boolokra is van rendezés
    }

    void ontudat(){
        klon = true;
    }

};

/**Lehet kívül is az operátor, de akkor két bemenet kell: (ebben még nincs benne a klónos rész)
   bool operator < (Ember E1, Ember E2){
       return E1.nev < E2.nev;
   }
*/

vector<Ember> rendez(vector<Ember> sor){ //BUBORÉK(?) RENDEZÉS: a két szomzédosat felcseréljük, ha rossz sorrendben vannak
    //Nem kell referencia szerint átvenni, mert a returnben adjuk oda a rendezett vektort

    for(int i = 1; i < sor.size(); i++){
        if(sor[i]  < sor[i-1]){//csere
            Ember E = sor[i-1];
            sor[i-1] = sor[i];
            sor[i] = E;
            i -= 2;
        }
    }

    return sor;
}

struct Varazslo{
    int* szam; //Ez azt jelenti, hogy int típusú memóriának fogja tárolni a címét

    Varazslo(int& Szam) {
        szam = & Szam;
        *szam = 0;
    }

    void szamol(vector<int> v){
        for(int a : v){
            *szam += a;
        }
    }
};



// Idaig

int main()
{
    int pont = 0;
    srand(time(0));

    Ember e1;
    ellenoriz(e1.nev == "Lajos", pont);

    Ember e2("Alajos");
    ellenoriz(e2.nev == "Alajos", pont);

    set<Ember> csoport;
    csoport.insert(e1);
    csoport.insert(e2);
    ellenoriz(csoport.size()==2, pont);

    Ember klon;
    csoport.insert(klon);
    ellenoriz(csoport.size()==2, pont);

    klon.ontudat();
    csoport.insert(klon);
    ellenoriz(csoport.size()==3 && klon.nev=="Lajos", pont);
    ellenoriz(csoport.size()==3 && klon.nev=="Lajos", pont);

    Ember e3("Zsolt");
    Ember e4("Balazs");
    vector<Ember> sor;
    sor.push_back(e3);
    sor.push_back(e4);
    sor.push_back(e1);
    sor.push_back(e2);
    vector<Ember> abecesor = rendez(sor);
    int r1 = rand()%(sor.size()-1);
    int r2 = rand()%(sor.size()-1);
    ellenoriz(abecesor[r1].nev < abecesor[r1+1].nev, pont);
    ellenoriz(abecesor[r2].nev < abecesor[r2+1].nev, pont);

///Ezt memóriacímek segítségével oldottam meg, de lehet van másik megoldása is
///Ha valamilyen változó elé &-t írsz, eléred a memóriacímét, ha egy változóban memóriacímet tárolsz, akkor az értéket *-gal éred el

    vector<int> v;
    int sum=0;
    for (int i=0;i<10;i++) {
        int r = rand()%100;
        v.push_back(r);
        sum+=r;
    }
    int valami;
    Varazslo wiz(valami);
    wiz.szamol(v);
    ellenoriz(valami == sum, pont);
    ellenoriz(valami == sum, pont);

    cout << endl << pont << "/10 pont";
}
