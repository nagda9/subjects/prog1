#include <iostream>
#include <graphics.hpp>
#include <vector>
#include <map>
#include <math.h>

using namespace genv;
using namespace std;

const int W = 600;
const int H = 600;

struct pont{
    int x;
    int y;

    pont(){
    }

    pont(int X, int Y){
        x = X;
        y = Y;
    }

    bool operator < (const pont P)const{
        if(x != P.x)
            return x < P.x;
        return y < P.y;
    }

    bool operator == (const pont P)const{
        return x == P.x && y == P.y;
    }
};

///Structon kívül is lehet az operátort:
//    bool operator < (pont P1, pont P2){
//        if(P1.x != P2.x)
//            return P1.x < P2.x;
//        return P1.y < P2.y;
//    }
//
//    bool operator == (pont P1, pont P2){
//        return P1.x == P2.x && P1.y == P2.y;
//    }

void torles(){
    gout << move_to(0,0) << color(0,0,0) << box(W,H);
}

int tavolsag(pont p1, pont p2){
    return sqrt(pow(p1.x-p2.x,2) + pow(p1.y-p2.y,2));
}

pont legmesszebbiPont(map<pont,int> minimum){
    int maxTav = 0;
    pont maxPont;
    for(pair<pont, int> p : minimum){
        if(p.second > maxTav){
            maxTav = p.second;
            maxPont = p.first;
        }
    }
    return maxPont;
}

void parkereses(map<pont, vector<pont>> & pontok){
    map<pont, vector<pont>> pontokUj;

    for(pair<pont, vector<pont>> p1 : pontok){ //Ezzel a forciklussal megnézzük egyesével a pontokat
        map<pont,int> minimum; //Ezekben tároljuk a legközelebbi pontokat és azok távolságát

        for(pair<pont, vector<pont>> p2 : pontok){//Ezzel a forciklussal kiválasztjuk az adott ponthoz tartozó legközelebbi 3 pontot
            if(p1.first == p2.first)
                continue; // amikor önmagával hasonlítom össze az elemet, akkor továbbléptetem a ciklust
            minimum[p2.first] = tavolsag(p1.first,p2.first);

            if(minimum.size() > 3){
                minimum.erase(minimum.find(legmesszebbiPont(minimum)));
            }
        }

        //Hozzáadjuk a ponthoz a legközelebbi 3-at
        for(pair<pont,int> m : minimum){
            pontokUj[p1.first].push_back(m.first);
        }
    }

    pontok = pontokUj;
}

void kirajzolo(map<pont,vector<pont>> pontok){
    gout << color(255,255,255);
    for(pair<pont,vector<pont>> P : pontok){
        for(pont p : P.second){
            gout << move_to(P.first.x, P.first.y) << line_to(p.x, p.y);
        }
    }
}

//void kiiro(map<pont,vector<pont>> pontok){
//    for(pair<pont,vector<pont>> P : pontok){
//        cout << P.first.x << " " <<P.first.y << ":\n";
//        for(pont p : P.second){
//            cout << "\t" << p.x << " " << p.y << endl;
//        }
//    }
//}

int main()
{
    gout.open(W,H);
    event ev;
    //Előre megadott pontok:
    pont p1(W/4,H/4);
    pont p2(W/4,3*H/4);
    pont p3(3*W/4,H/4);
    pont p4(3*W/4,3*H/4);
    pont p5(10,50);

    map<pont,vector<pont>> pontok;
    //Ezzel feltöltjük a kulcsokat, de a secondok(vector<pont> -ok) üresek lesznek (0 elemű vektorok)
    pontok[p1];
    pontok[p2];
    pontok[p3];
    pontok[p4];
    pontok[p5];

    parkereses(pontok);
//    kiiro(pontok); //Debuggoláshoz
    kirajzolo(pontok);
    gout << refresh;

    while(gin >> ev && ev.keycode != key_escape){
        if(ev.button == btn_left){
            pont p(ev.pos_x, ev.pos_y);
            pontok[p];
            parkereses(pontok);

            torles();
            kirajzolo(pontok);
            gout << refresh;
        }
    }
    return 0;
}
