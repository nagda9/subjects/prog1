#include <graphics.hpp>
#include <iostream>
#include <vector>
#include <math.h>

using namespace std;
using namespace genv;

const int W = 600; //Nem muszáj a const
const int H = 600;

// polárkordinátás kör
// polár koordinátával az a baj, hogy ha nagy a sugár, akkor nem lesz összefüggő az alakzat, mivel 0-360 fokig nem elég nagy a felbontás int-ekkel
void pont(int X, int Y){//x,y kordinátára rak egy fekete pöttyöt
    gout << color(0,0,0);
    for(int r = 0; r <= 4; r++)	//A kör sugara
    {
        for(int fi = 0; fi < 360; fi++)
        {
        	int x = r*cos(fi);
        	int y = r*sin(fi);
            gout << move_to(X+x, Y+y) << dot;
        }
    }
}

double tavolsag(int x1, int y1, int x2, int y2){ //Megadja két pont távolságát
    return sqrt(pow(x1-x2,2) + pow(y1-y2,2));
}

vector<int> origo(int x1, int y1, int x2, int y2)
{
	vector<int> origo (2);
	origo[0] = ceil((x1 + x2)/2);
	origo[1] = ceil((y1 + y2)/2);
	return origo;
}

void ellipszisRajzolo(int x1, int y1, int x2, int y2){//A két pont x és y koordinátája
    double d = tavolsag(x1,y1,x2,y2);
    double ossz = 1.5*d;

    gout << color(255,255,255);
    //Itt lehetne kiszámolni, hogy mettől meddig menjen a forciklus, mert csak egy bizonyos téglalapon belül lessznek fehér pontok (mint a pont()-nál),
    //de ha az egész képernyő összes pontját átvizsgálod az se baj, csak kicsit lassabb lesz
	
	vector<int> mid = origo(x1, y1, x2, y2);
	

    
     for(int i = ceil(mid[0] - ossz/2); i <= ceil(mid[0] + ossz/2); i++){
        for(int j = ceil(mid[1] - ossz/2); j <= ceil(mid[1] + ossz/2); j++){
            if(tavolsag(x1,y1,i,j) + tavolsag(x2,y2,i,j) <= ossz){//Ellipszis egyenlete
                gout << move_to(i,j) << dot;
            }
        }
    }
    
    // sikertelen kísérlet polárra
    /*for(int r = 0; r <= ossz; r++)
	{
		for(int fi = 0; fi < 360; fi++)
		{
			if (r = (ossz/2*(1-pow(d/ossz, 2)))/(1+(d/ossz*cos(fi))))
			{
				int x = r*cos(fi);
        		int y = r*sin(fi);
				gout << move_to(x1+x,y1+y) << dot;
			}
		}
	}*/
}

void torles(){
    gout << move_to(0,0) << color(0,0,0) << box(W,H);
}

int main()
{
    gout.open(W,H);

    int x1 = W/2;
    int y1 = H/2;

	event ev;
    while(gin>> ev && ev.keycode != key_escape){
        if(ev.type == ev_mouse)
        {
            int x2 = ev.pos_x;
            int y2 = ev.pos_y;
            torles();
            ellipszisRajzolo(x1,y1,x2,y2);
            pont(x1, y1);
            pont(x2, y2);
            gout << refresh;
        }
    }

    return 0;
}
