#include <iostream>
#include <vector>
#include <set>
#include <map>

using namespace std;

struct Projekt
{
 private:
   //kulcs a részfeladat neve, érték a résztvevők neveinek halmaza 
   map<string, set<string>> reszfeladatok; 
   
   /*kulcs az időpont (a munka hányadik napja), érték azoknak a 
     részfeladatoknak a halmaza, amikre van találkozó az adott napra*/
   map<int, set<string>> naptar;
   
 public:
   void uj_reszfeladat(string feladat)
     {
	//új részfeladatot ad hozzá (make_pair egy pair-rel tér vissza)
	reszfeladatok.insert(make_pair(feladat, set<string>()));
	
	/*nem kell ellenőrizni, hogy az adott részfeladat 
	  szerepel-e már a részfeladatok "listájában", mivel 
	 úgyis az eredetit hagyja meg.*/
     }
   
   void kulcsember(string feladat, string nev)
     {
	//új kulcsembert ad egy részfeladathoz
	reszfeladatok[feladat].insert(nev);	
     }
  
   bool uj_talalkozo(string feladat, int nap, string & elfoglalt)
     {
	/*megkeresi az időpontütközéseket; ha talál, akkor igaz a 
	 * visszatérési érték, és az érintett ember nevét beleírja az 
	 * elfoglalt nevű változóba; egyéb esetekben a visszatérési
	 * érték hamis */
	
	/*ellenőrzi, hogy az adott napra az adott részfeladathoz nincs-e
	 * már találkozó*/
	if (naptar[nap].find(feladat) != naptar[nap].end())
	  {
	     cout << "Mar van talalkozo ehhez a feladathoz\n";
	     
	     return false;
	  }
	
	//végignézi a nap időpontra szóló naptárbejegyzéseket
	for (string f: naptar[nap])
	  {
	     //és a naptárbejegyzésekhez tartozó kulcsembereket
	     for (string ember: reszfeladatok[f])
	       {
		  /*és megnézi, hogy a kulcsember a hozzáadandó 
		   * feladatnak is kulcsembere-e*/
		  if (reszfeladatok[feladat].find(ember) != 
		         reszfeladatok[feladat].end())
		    {
		       elfoglalt = ember;
		       
		       return true;
		    }
	       }
	     
	  }
	
	//ha nincs ütközés, a találkozót beírja a naptárba
	naptar[nap].insert(feladat);
	
	return false;
     }
   
   void teljes_naptar_kiir()
     {
	//végigmegy a naptáron
	for (pair<int, set<string>> p: naptar)
	  {
	     cout << p.first << ": " << endl;
	     
	     //és az adott napra bejegyzett találkozókon
	     for (string s: p.second)
	       cout << "\t" << s << endl;
	  }
     }
};

int main()
{
   Projekt p;
   
   p.uj_reszfeladat("karbantartas");
   p.uj_reszfeladat("tervezes");
   p.uj_reszfeladat("marketing");
   
   p.kulcsember("karbantartas", "Tibi");
   p.kulcsember("tervezes", "Tibi");
   p.kulcsember("tervezes", "Zsofi");
   p.kulcsember("marketing", "Lajos");
   
   string elfoglalt;
   
   if (p.uj_talalkozo("tervezes", 12, elfoglalt))
     cout << elfoglalt << " nem er ra.\n";
   
   if (p.uj_talalkozo("marketing", 12, elfoglalt))
     cout << elfoglalt << " nem er ra.\n";
   
   if (p.uj_talalkozo("tervezes", 13, elfoglalt))
     cout << elfoglalt << " nem er ra.\n";//részfeladat neve a kulcs, 
   
   if (p.uj_talalkozo("karbantartas", 13, elfoglalt))
     cout << elfoglalt << " nem er ra.\n";
   
   p.teljes_naptar_kiir();
   
   return 0;
}
