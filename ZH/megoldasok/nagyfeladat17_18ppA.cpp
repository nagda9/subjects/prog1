/**
*	A C része hiányzik a zhk-nak és a megoldások nem feltétlenül a legjobbak.
*	Egy feladatot több féle képpen is meg lehet oldani, ha a tiéd nem ilyen de működik,
*	akkor az tökéletes. Ha hibát találtok légyszi szóljatok.
*	Hajrá a zh-n!
*/

#include <iostream>
#include <vector>
#include <list>
#include <cmath>

using namespace std;

struct koord{
    int x;
    int y;

    koord(int X, int Y) {
        x = X;
        y = Y;
    }
};

struct Lepes
{
private:
public:
    list<koord> lepesek;
    Lepes(int x, int y)
    {
        koord k(x,y);
        lepesek.push_back(k);
    }

    void lep(int x, int y)
    {
        koord k(x,y);
        lepesek.push_back(k);
    }


    list<koord> getLepesek() {
        return lepesek;
    }
};

struct Dama
{
private:
    vector<koord> babuk;

public:
    void uj_babu(int x, int y)
    {
        koord k(x,y);
//        k.x = x;
//        k.y = y;
        babuk.push_back(k);
    }

    bool szabalyos_e(Lepes lp)
    {
        //cout <<"lpsize: " << lp.lepesek.size() << endl;
        list<koord>::iterator elso = lp.lepesek.begin();
        list<koord>::iterator masodik = lp.lepesek.begin();
        masodik++;
        while(masodik != lp.lepesek.end()) {
            int x1,x2,y1,y2;
            x1 = (*elso).x;
            x2 = (*masodik).x;
            y1 = (*elso).y;
            y2 = (*masodik).y;
            if(abs(x2-x1) == (y2-y1) && 0 < (y2-y1) && (y2-y1) <=2) {
                if(y2-y1 == 2) { //Ha ugrik
                    if(!van_e_babu(x1+((x2-x1)/2), y2-1)) {
                        return false;
                    }
                }
                if(van_e_babu(x2,y2)) {
                    return false;
                }
            }
            else {return false;}
            elso++;
            masodik++;
        }
        return true;
    }

    bool van_e_babu(int x, int y) {
        for(koord b : babuk) {
            if(b.x == x && b.y == y) {
                return true;
            }
        }
        return false;
    }

    void meglep(Lepes lp){
        list<koord>::iterator elsoElem = lp.lepesek.begin();
        list<koord>::iterator utolsoElem = lp.lepesek.end();
        utolsoElem--;

        for(unsigned i = 0; i < babuk.size(); i++){

            //Ha megtaláljuk a bábút, amivel lépni akarunk...
            if(babuk[i].x == (*elsoElem).x && babuk[i].y == (*elsoElem).y){ //Ezzel magát az első elemet kérjük el

                //... lépjünk vele.
                babuk[i] = *utolsoElem;
                return;
            }
        }
    }

    void kiir() {
        cout << "\nBabuk:\n";
        for(koord k : babuk) {
            cout << k.x << " " << k.y << "\n";
        }
    }

};


int main()
{
    Dama d;
    d.uj_babu(1,1); ///x,y koordináták, az y nő az ellenfél felé
    d.uj_babu(0,0);
    d.uj_babu(2,0);
    Lepes lp(1,1); /// az 1,1 koordinátán álló bábu akar majd lépni
    lp.lep(2,2);
    cout << d.szabalyos_e(lp); /// "1",  szabályos lépés, egy átlós előre lépés. A szabályosságvizsgálat NEM lépi meg a lépést
    Lepes lp2(0,0);
    lp2.lep(2,2);
    cout << d.szabalyos_e(lp2); /// "1",  szabályos ugrás, egy saját bábut (1,1) átlós előre átugrás.
    d.uj_babu(1,3);
    lp2.lep(0,4); /// lp2 ugrássorozat folytatódik
    cout << d.szabalyos_e(lp2); /// "1",  szabályos ugrássorozat, az (1,1)-et átugorva (2,2)-re, onnan (1,3)-at átugorva (0,4)-re érkezünk
    d.kiir();
    d.meglep(lp); /// az lp lépés megtétele
    d.kiir();
    cout << d.szabalyos_e(lp2); /// "0",  szabálytalan ugrássorozat, az (1,1)-et átugorva juthatnánk (2,2)-re, de az (1,1)-en már nincs bábu
    Lepes lp3(2,2);
    lp3.lep(3,3);
    cout << d.szabalyos_e(lp3); /// "1"
    return 0;
}

