#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <string>

using namespace std;

struct Offshore
{
 private:
   //abc rendben tárolja a cég nevét és a hozzá tartozó országot
   map<string, string> cegek;
   
   //abc rendben eltárolja a személyek neveit
   set<string> szemelyek;
   
   //adott országhoz eltárolja a cégeket, és a cégekhez a személyek neveit
   map<string, map<string, set<string>>> orszagok;
   
 public:
   void uj_ceg(string ceg, string orszag)
     {
	cegek.insert(make_pair(ceg, orszag));
	orszagok.insert(make_pair(orszag, map<string, set<string>>()));
     }
   
   void uj_szemely(string szemely)
     {
	szemelyek.insert(szemely);
     }
   
   void ceg_szemelyhez(string szemely, string ceg)
     {
	if (cegek.find(ceg) != cegek.end())
	  {
	     if (orszagok[cegek[ceg]].find(szemely) == 
		    orszagok[cegek[ceg]].end())
	       orszagok[cegek[ceg]].insert(make_pair(szemely, set<string>()));
	     orszagok[cegek[ceg]][szemely].insert(ceg);
	  }
     }
   
   void ujsagiro(string orszag)
     {
	for (pair<string, set<string>> szemely: orszagok[orszag])
	  {
	     cout << szemely.first << ":\n";
	     for (string ceg: orszagok[orszag][szemely.first])
	       cout << "\t" << ceg << endl;
	  }
     }

   void osszes_szemely()
     {
	cout << "Összes személy:\n";
	for (string szemely: szemelyek)
	  cout << "\t" << szemely << endl;
     }
   
   void osszes_ceg()
     {
	cout << "Összes cég:\n";
	for (pair<string, string> ceg: cegek)
	  cout << "\t" << ceg.first << endl;
     }
};

int main()
{
   Offshore os;
   
   os.uj_ceg("ACME", "USA");
   os.uj_ceg("Adibas", "Nemetorszag");
   os.uj_ceg("Inke", "Kina");
   os.uj_ceg("On shore", "Magyarorszag");
   
   os.uj_szemely("Lovag Endre");
   os.uj_szemely("Li Li Li");
   os.uj_szemely("Joseph Ritter");
   os.uj_szemely("Jack Knight");
   os.uj_szemely("Peter Redlich");
   os.uj_szemely("Xiang Huong Peng");
   os.uj_szemely("Joo Janos");
   
   os.ceg_szemelyhez("Lovag Endre", "On shore");
   os.ceg_szemelyhez("Joseph Ritter", "Adibas");
   os.ceg_szemelyhez("Peter Redlich", "Adibas");
   os.ceg_szemelyhez("Li Li Li", "Inke");
   os.ceg_szemelyhez("Xiang Huong Peng", "Inke");
   os.ceg_szemelyhez("Joo Janos", "On shore");
   os.ceg_szemelyhez("Jack Knight", "ACME");
   os.ceg_szemelyhez("Jack Knight", "On shore");
   os.ceg_szemelyhez("Jack Knight", "Inke");
   
   os.ujsagiro("Nemetorszag");
   
   os.osszes_szemely();
   os.osszes_ceg();
   
   return 0;
}
