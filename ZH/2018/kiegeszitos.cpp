#include <iostream>
#include <cstdlib>
#include <ctime>
#include <cmath>
#include <vector>
#include <fstream>
#include <set>
#include <map>
#include <algorithm>
using namespace std;

#define ellenoriz(felt, pont) if (felt) pont++; else cerr << "Nem teljesul a feltetel a " << __LINE__<< ". sorban." <<endl;

/* Szab�lyok:
    - csak az a beugr� �rv�nyes, amely nem tartalmaz semmilyen v�ltoztat�st a main()-ben,
        �s m�r nincs benne kommentezve semmi az "//idaig" sor ut�n
    - az "ellenoriz"-en k�v�l nem tartalmaz #define sorokat, �s azt sem szabad megv�ltoztatni
    - �j #include megengedett a C/C++ standard k�nyvt�rbeli elemek k�z�l
*/

// Innentol
struct Ember
{
	string nev;
	bool klon;
	
	Ember()
	{
		nev = "Lajos";
		klon = false;
	}
	
	Ember(string N)
	{
		nev = N;
		klon = false;
	}
	
	bool operator < (const Ember masik) const
	{
		if (nev != masik.nev)
		{
			return nev < masik.nev;
		}
		return klon < masik.klon;
	}
	
	void ontudat()
	{
		klon = true;
	}
};

vector<Ember> rendez(vector<Ember> sor)
{
	sort(sor.begin(), sor.end());
	return sor;
}

struct Varazslo
{
	int* x; // int t�pus� mem�riac�m, �s nem pedig v�lktoz�, teh�t a c�mre hivatkozva tudunk �rt�ket adni
	
	Varazslo(int& valami)
	{
		x = & valami; // x megkapja a mem�riac�met
		*x = 0;
	}
	
	void szamol(vector<int> v)
	{
		for(int n : v)
		{
			*x += n; 
		}
	}
};

// Idaig

int main()
{
    int pont = 0;
    srand(time(0));

    Ember e1;
    ellenoriz(e1.nev == "Lajos", pont);

    Ember e2("Alajos");
    ellenoriz(e2.nev == "Alajos", pont);

    set<Ember> csoport;
    csoport.insert(e1);
    csoport.insert(e2);
    ellenoriz(csoport.size()==2, pont);

    Ember klon;
    csoport.insert(klon);
    ellenoriz(csoport.size()==2, pont);

    klon.ontudat();
    csoport.insert(klon);
    ellenoriz(csoport.size()==3 && klon.nev=="Lajos", pont);
    ellenoriz(csoport.size()==3 && klon.nev=="Lajos", pont);

    Ember e3("Zsolt");
    Ember e4("Balazs");
    vector<Ember> sor;
    sor.push_back(e3);
    sor.push_back(e4);
    sor.push_back(e1);
    sor.push_back(e2);
    vector<Ember> abecesor = rendez(sor);
    int r1 = rand()%(sor.size()-1);
    int r2 = rand()%(sor.size()-1);
    ellenoriz(abecesor[r1].nev < abecesor[r1+1].nev, pont);
    ellenoriz(abecesor[r2].nev < abecesor[r2+1].nev, pont);

    vector<int> v;
    int sum=0;
    for (int i=0;i<10;i++) {
        int r = rand()%100;
        v.push_back(r);
        sum+=r;
    }
    int valami;
    Varazslo wiz(valami);
    wiz.szamol(v);
    ellenoriz(valami == sum, pont);
    ellenoriz(valami == sum, pont);

    cout << endl << pont << "/10 pont";
}
