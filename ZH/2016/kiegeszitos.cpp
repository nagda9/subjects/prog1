#include <iostream>
#include <sstream>
#include <vector>
#include <map>
#include <algorithm>

using namespace std;

#define ellenoriz(felt, pont) if (felt) pont++; else cerr << "Nem teljesul a feltetel a " << __LINE__<< ". sorban." <<endl;

/* Szabalyok:
    - csak az a kieg�sz�tos feladat �rv�nyes, amely nem tartalmaz semmilyen v�ltoztat�st a main()-ben,
        �s m�r nincs benne kommentezve semmi
    - az "ellenoriz"-en k�v�l nem tartalmaz #define sorokat, �s azt sem szabad megv�ltoztatni
*/

// Innentol
struct A{
	int x;
	
	A()
	{
		x = 42;
	}
	
	A(int n)
	{
		x = n;
	}
	
	bool operator <(const A masik) const
	{
		return x < masik.x;
	}
};

ostream& operator << (ostream& os, A a)
{
	os << a.x;
	return os;
}

struct B
{
	string s1, s2;
	
	B(string S1, string S2)
	{
		s1 = S1;
		s2 = S2;
	}
	
	bool operator == (const B masik) const
	{
		return s1 == masik.s1 && s2 == masik.s2;	
	}
	
	bool operator != (const B masik) const
	{
		return s1 != masik.s1 || s2 != masik.s2;	
	}
	
	bool operator < (const B masik) const
	{
		return s1 < masik.s1;
	}
};
// Idaig


int main()
{
    int pont = 0;

    A a;
    ellenoriz(a.x==42 , pont)

    A b(1337);
    ellenoriz(b.x==1337 , pont)

    vector<A> v;
    v.push_back(A(3));
    v.push_back(A(2));
    v.push_back(A(1));
    sort(v.begin(), v.end());
    ellenoriz(v[0].x==1 && v[1].x==2 && v[2].x==3, pont)

    stringstream ss;
    ss << a;
    ellenoriz(ss.str()=="42", pont)

    stringstream ss2;
    ss2 << a << "!";
    ellenoriz(ss2.str()=="42!", pont)

    B b1("ab","cd");
    B b1m("ab","cd");
    B b2("abc","d");
    ellenoriz(b1m==b1 && !(b1==b2), pont)
    ellenoriz(b1!=b2, pont)

    map<B, int> m;
    m[b1]=1;
    m[b2]=2;
    m[b1m]=3;

    ellenoriz(m.at(b2)==2, pont)
    ellenoriz(m.size()==2, pont)
    ellenoriz(m.at(b1)==3, pont)

    cout << pont << " / 10 pont" << endl;
    return 0;
}
